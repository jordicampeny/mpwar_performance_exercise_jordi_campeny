<?php

namespace Performance\Infrastructure\Repositories;

use Performance\Domain\Article;
use Performance\Domain\ArticleRepository;
use Predis\Client;

final class ArticlesCacheRepository implements ArticleRepository
{
    const ARTICLE_BY_ID = 'article:id:';
    const ALL_ARTICLES  = 'articles';
    const TTL           = 1000;

    private $diskRepository;
    private $predisClient;

    public function __construct(
        ArticleRepository $diskRepository,
        Client $client
    ) {
        $this->diskRepository = $diskRepository;
        $this->predisClient   = $client;
    }

    /**
     * @param Article $article
     */
    public function save(Article $article)
    {
        $this->diskRepository->save($article);

        $this->deleteArticlesFromCache($article);

        $this->persistInCache(
            $article,
            $this->getArticleKey($article)
        );
    }

    /**
     * @param $article_id
     * @return null|Article
     */
    public function findOneById($article_id)
    {
        $articleKey = $this->getArticleKey($article_id);

        if ($articleFromCache = $this->getArticleFromCache($articleKey)) {
            return $articleFromCache;
        }

        $article = $this->diskRepository->findOneById($article_id);
        $this->persistInCache(
            $article,
            $articleKey
        );

        return $article;
    }

    public function findAll()
    {
        if ($articleSFromCache = $this->getArticleFromCache(self::ALL_ARTICLES)) {
            return $articleSFromCache;
        }

        $articles = $this->diskRepository->findAll();
        $this->persistInCache(
            $articles,
            self::ALL_ARTICLES
        );

        return $articles;
    }

    /**
     * @param Article $article
     */
    private function deleteArticlesFromCache(Article $article)
    {
        $this->deleteKey($this->getArticleKey($article));
        $this->deleteKey(self::ALL_ARTICLES);
    }

    /**
     * @param $articleCacheKey
     */
    private function deleteKey($articleCacheKey)
    {
        if ($this->predisClient->exists($articleCacheKey)) {
            $this->predisClient->del($articleCacheKey);
        }
    }

    /**
     * @param Article | array $article
     * @param String $articleKey
     */
    private function persistInCache($article, String $articleKey)
    {
        $serializedArticle = serialize($article);

        $this->predisClient->set($articleKey, $serializedArticle);
        $this->predisClient->expire($articleKey, self::TTL);
    }

    /**
     * @param Article | String $article
     * @return string
     */
    private function getArticleKey($article)
    {
        if (is_string($article)) {
            return self::ARTICLE_BY_ID . $article;
        }

        return self::ARTICLE_BY_ID . $article->getId();
    }

    private function getArticleFromCache($articleKey)
    {
        if ($this->predisClient->exists($articleKey)) {
            $articleCached = $this->predisClient->get($articleKey);

            if (isset($articleCached)) {
                return unserialize($articleCached);
            }
        }

        return null;
    }
}