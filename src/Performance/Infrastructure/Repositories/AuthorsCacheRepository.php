<?php

namespace Performance\Infrastructure\Repositories;

use Performance\Domain\Author;
use Performance\Domain\AuthorRepository;
use Predis\Client;

final class AuthorsCacheRepository implements AuthorRepository
{
    const USER_ID  = 'user:id:';
    const USER_NAME = 'user:name:';
    const TTL       = 1000;

    private $diskRepository;
    private $predisClient;

    public function __construct(
        AuthorRepository $diskRepository,
        Client $client
    ) {
        $this->diskRepository = $diskRepository;
        $this->predisClient   = $client;
    }


    /**
     * @param Author $author
     */
    public function save(Author $author)
    {
        $this->diskRepository->save($author);

        $this->persistInCache($author);
    }

    /**
     * @param $author_id
     * @return null|Author
     * @internal param $userId
     */
    public function findOneById($author_id)
    {
        $authorKey = $this->getAuthorIdKey($author_id);

        if ($authorFromCache = $this->getAuthorFromCache($authorKey)) {
            return $authorFromCache;
        }

        $author = $this->diskRepository->findOneById($author_id);
        $this->persistInCache($author);

        return $author;
    }

    /**
     * @param $username
     * @return null|Author
     */
    public function findOneByUsername($username)
    {
        $authorKey = $this->getAuthorNameKey($username);

        if ($authorFromCache = $this->getAuthorFromCache($authorKey)) {
            return $authorFromCache;
        }

        $author = $this->diskRepository->findOneByUsername($username);
        $this->persistInCache($author);

        return $author;
    }

    /**
     * @param Author $author
     */
    private function persistInCache(Author $author)
    {
        $keys = [
            $this->getAuthorIdKey($author->getId()),
            $this->getAuthorNameKey($author->getUsername())
        ];

        foreach ($keys as $key) {
            $serializedAuthor = serialize($author);

            $this->predisClient->set($key, $serializedAuthor);
            $this->predisClient->expire($key, self::TTL);
        }
    }

    /**
     * @param string $authorId
     * @return string
     */
    private function getAuthorIdKey(string $authorId)
    {
        return self::USER_ID . $authorId;
    }

    /**
     * @param string $authorName
     * @return string
     */
    private function getAuthorNameKey(string $authorName)
    {
        return self::USER_NAME . $authorName;
    }
    /**
     * @param string $authorKey
     * @return mixed|null
     */
    private function getAuthorFromCache(string $authorKey)
    {
        if ($this->predisClient->exists($authorKey)) {
            $authorCached = $this->predisClient->get($authorKey);

            if (isset($authorCached)) {
                return unserialize($authorCached);
            }
        }

        return null;
    }
}