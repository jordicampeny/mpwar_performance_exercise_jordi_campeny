<?php

namespace Performance\Infrastructure\Repositories;

use Performance\Domain\ArticleRepository;
use Performance\Domain\Ranking;
use Performance\Domain\RankingRepository;
use Predis\Client;

final class RankingCacheRepository implements RankingRepository
{
    const TOP_ARTICLES        = "articles:top";
    const TOP_ARTICLES_AUTHOR = "articles:top:author:id:";

    const TTL = 3000000;

    const RANKING_START_AT = 0;

    private $predisClient;
    private $articleRepository;

    /**
     * RankingCacheRepository constructor.
     * @param Client $client
     * @param ArticleRepository $articleRepository
     */
    public function __construct(
        ArticleRepository $articleRepository,
        Client $client
    ) {
        $this->articleRepository = $articleRepository;
        $this->predisClient      = $client;
    }

    /**
     * @param int $quantity
     * @return Ranking
     */
    public function getTop(int $quantity)
    {
        return $this->getRankingFromKey(self::TOP_ARTICLES, $quantity);
    }

    /**
     * @param string $authorId
     * @param int $quantity
     * @return Ranking
     */
    public function getAuthorTop($authorId, int $quantity)
    {
        if (is_null($authorId)) {
            return Ranking::create([]);
        }

        return $this->getRankingFromKey($this->getAuthorTopKey($authorId), $quantity);
    }

    /**
     * @param string $articleId
     * @param string $authorId
     * @param int $quantity
     * @return mixed
     */
    public function increaseVisitsIn(
        string $articleId,
        string $authorId,
        int $quantity = 1
    ) {
        $this->predisClient->zincrby(
            self::TOP_ARTICLES,
            $quantity,
            $articleId
        );
        $this->predisClient->zincrby(
            $this->getAuthorTopKey($authorId),
            $quantity,
            $articleId
        );

        $this->predisClient->expire(self::TOP_ARTICLES, self::TTL);
        $this->predisClient->expire($this->getAuthorTopKey($authorId), self::TTL);
    }

    /**
     * @param string $userId
     * @return string
     */
    private function getAuthorTopKey(string $userId): string
    {
        return self::TOP_ARTICLES_AUTHOR . $userId;
    }

    /**
     * @param string $key
     * @param int $quantity
     * @return Ranking
     */
    private function getRankingFromKey(string $key, int $quantity): Ranking
    {
        $topArticles = [];

        $articleIds = $this->predisClient->zrevrange(
            $key,
            self::RANKING_START_AT,
            $quantity - 1
        );

        foreach ($articleIds as $articleId) {
            $topArticles[] = $this->articleRepository->findOneById($articleId);
        }

        return Ranking::create($topArticles);
    }
}