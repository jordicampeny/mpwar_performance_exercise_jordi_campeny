<?php

namespace Performance;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class DomainServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['S3.client'] = function () use ($app) {
            return new \Aws\S3\S3Client([
                'credentials' => [
                    'key' => $app["S3.client.credentials.key"],
                    'secret' => $app["S3.client.credentials.secret"],
                ],
                'region' => $app["S3.client.region"],
                'version' => $app["S3.client.version"],
            ]);
        };

        $app['Predis.Client'] = function () use ($app) {
            return new \Predis\Client([
                'scheme' => $app['redis.client.scheme'],
                'host' => $app['redis.client.host'],
                'port' => $app['redis.client.port']
            ]);
        };

        $app['AwS3Adapter'] = function () use ($app) {
            return new \League\Flysystem\AwsS3v3\AwsS3Adapter(
                $app['S3.client'],
                $app['S3.bucket']
            );
        };

        $app['FileSystem'] = function () use ($app) {
            return new \League\Flysystem\Filesystem($app['AwS3Adapter']);
        };

        $app['useCases.signUp'] = function () use ($app) {
            return new \Performance\Domain\UseCase\SignUp($app['authors.cache.repository']);
        };

        $app['useCases.login'] = function () use ($app) {
            return new \Performance\Domain\UseCase\Login(
                $app['authors.cache.repository'],
                $app['session']
            );
        };

        $app['useCases.writeArticle'] = function () use ($app) {
            return new \Performance\Domain\UseCase\WriteArticle(
                $app['articles.cache.repository'],
                $app['authors.cache.repository'],
                $app['session']
            );
        };

        $app['useCases.editArticle'] = function () use ($app) {
            return new \Performance\Domain\UseCase\EditArticle(
                $app['articles.cache.repository'],
                $app['authors.cache.repository'],
                $app['session']
            );
        };

        $app['useCases.readArticle'] = function () use ($app) {
            return new \Performance\Domain\UseCase\ReadArticle(
                $app['articles.cache.repository'],
                $app['ranking.cache.repository']
            );
        };

        $app['useCases.listArticles'] = function () use ($app) {
            return new \Performance\Domain\UseCase\ListArticles($app['articles.cache.repository']);
        };

        $app['useCases.getTopArticles'] = function () use ($app) {
            return new \Performance\Domain\UseCase\GetTopArticles(
                $app['ranking.cache.repository'],
                $app['articles.cache.repository']
            );
        };


        $app['controllers.readArticle'] = function () use ($app) {
            return new \Performance\Controller\ArticleController(
                $app['twig'],
                $app['useCases.readArticle']
            );
        };

        $app['controllers.writeArticle'] = function () use ($app) {
            return new \Performance\Controller\WriteArticleController(
                $app['twig'],
                $app['url_generator'],
                $app['useCases.writeArticle'],
                $app['session']
            );
        };

        $app['controllers.editArticle'] = function () use ($app) {
            return new \Performance\Controller\EditArticleController(
                $app['twig'], $app['url_generator'],
                $app['useCases.editArticle'],
                $app['useCases.readArticle'],
                $app['session']
            );
        };

        $app['controllers.login'] = function () use ($app) {
            return new \Performance\Controller\LoginController(
                $app['twig'],
                $app['url_generator'],
                $app['useCases.login'],
                $app['session']
            );
        };

        $app['controllers.signUp'] = function () use ($app) {
            return new \Performance\Controller\RegisterController(
                $app['twig'],
                $app['url_generator'],
                $app['useCases.signUp'],
                $app['FileSystem']
            );
        };

        $app['controllers.home'] = function () use ($app) {
            return new \Performance\Controller\HomeController(
                $app['twig'],
                $app['useCases.listArticles'],
                $app['useCases.getTopArticles'],
                $app['session']
            );
        };

        $app['articles.cache.repository'] = function () use ($app) {
            return new \Performance\Infrastructure\Repositories\ArticlesCacheRepository(
                $app['orm.em']->getRepository('Performance\Domain\Article'),
                $app['Predis.Client']
            );
        };

        $app['authors.cache.repository'] = function () use ($app) {
            return new \Performance\Infrastructure\Repositories\AuthorsCacheRepository(
                $app['orm.em']->getRepository('Performance\Domain\Author'),
                $app['Predis.Client']
            );
        };

        $app['ranking.cache.repository'] = function () use ($app) {
            return new \Performance\Infrastructure\Repositories\RankingCacheRepository(
                $app['orm.em']->getRepository('Performance\Domain\Article'),
                $app['Predis.Client']
            );
        };

        /*
        $app['session.storage.handler'] = <STORAGE HANDLER HERE>;
        */
    }
}