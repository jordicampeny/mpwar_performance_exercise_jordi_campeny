<?php

namespace Performance\Controller;

use League\Flysystem\FilesystemInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Performance\Domain\UseCase\SignUp;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RegisterController
{
    /**
     * @var \Twig_Environment
     */
    private $template;

    /**
     * @var UrlGeneratorInterface
     */
    private $url_generator;

    /**
     * @var SignUp
     */
    private $useCase;

    public function __construct(
        \Twig_Environment $templating,
        UrlGeneratorInterface $url_generator,
        SignUp $useCase,
        FilesystemInterface $fileSystem
    ) {
        $this->template      = $templating;
        $this->url_generator = $url_generator;
        $this->useCase       = $useCase;
        $this->fileSystem    = $fileSystem;
    }

    public function get()
    {
        return new Response(
            $this->template->render('register.twig'),
            200,
            ['Cache-Control' => 's-maxage=50, private']
        );
    }

    public function post(Request $request)
    {
        $username     = $request->request->get('username');
        $password     = $request->request->get('password');
        $profileImage = $request->files->get('profileimage');

        $profileImageName = md5($username) . ".jpg";

        if ($this->fileSystem->has($profileImageName)) {
            $this->fileSystem->delete($profileImageName);
        }

        $this->fileSystem->writeStream(
            $profileImageName,
            fopen($profileImage->getRealPath(), 'r')
        );

        $this->useCase->execute($username, $password);

        return new RedirectResponse($this->url_generator->generate('login'));
    }
}