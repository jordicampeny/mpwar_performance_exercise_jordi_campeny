<?php

namespace Performance\Controller;

use Performance\Domain\UseCase\GetTopArticles;
use Symfony\Component\HttpFoundation\Response;
use Performance\Domain\UseCase\ListArticles;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class HomeController
{
    /**
     * @var \Twig_Environment
     */
    private $template;
    /**
     * @var GetTopArticles
     */
    private $getTopArticles;
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        \Twig_Environment $templating,
        ListArticles $useCase,
        GetTopArticles $getTopArticles,
        SessionInterface $session
    ) {
        $this->template       = $templating;
        $this->useCase        = $useCase;
        $this->getTopArticles = $getTopArticles;
        $this->session        = $session;
    }

    public function get()
    {
        $topArticles = $this->getTopArticles->execute($this->session->get('author_id'));
        $articles    = $this->useCase->execute();

        return new Response(
            $this->template->render('home.twig', [
                'articles' => $articles,
                'topArticles' => $topArticles->getTopArticles(),
                'topAuthorArticles' => $topArticles->getTopAuthorArticles()
            ]),
            200,
            ['Cache-Control' => 's-maxage=50, private']
        );
    }
}