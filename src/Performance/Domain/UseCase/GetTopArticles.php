<?php


namespace Performance\Domain\UseCase;


use Performance\Domain\ArticleRepository;
use Performance\Domain\RankingCollection;
use Performance\Domain\RankingRepository;

final class GetTopArticles
{
    const TOP_RANKING_QUANTITY        = 5;
    const TOP_AUTHOR_RANKING_QUANTITY = 5;

    private $rankingRepository;
    private $articleRepository;

    public function __construct(
        RankingRepository $rankingRepository,
        ArticleRepository $articleRepository
    ) {
        $this->rankingRepository = $rankingRepository;
        $this->articleRepository = $articleRepository;
    }

    public function execute(string $authorId = null): RankingCollection
    {
        $rankingCollection = new RankingCollection();

        $rankingCollection->setTopArticles(
            $this->rankingRepository->getTop(self::TOP_RANKING_QUANTITY)
        );
        $rankingCollection->setTopAuthorArticles(
            $this->rankingRepository->getAuthorTop(
                $authorId,
                self::TOP_AUTHOR_RANKING_QUANTITY
            )
        );

        return $rankingCollection;
    }
}