<?php


namespace Performance\Domain;


final class Ranking
{
    private $articles;

    /**
     * Ranking constructor.
     * @param Article[] $articles
     */
    private function __construct(array $articles = [])
    {
        $this->articles = $articles;
    }

    /**
     * @param Article[] $articles
     * @return Ranking
     */
    public static function create(array $articles = []): Ranking
    {
        return new self($articles);
    }

    /**
     * @return Article[] $articles
     */
    public function articles(): array
    {
        return $this->articles;
    }
}