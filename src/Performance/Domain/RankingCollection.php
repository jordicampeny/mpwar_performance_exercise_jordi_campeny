<?php


namespace Performance\Domain;


final class RankingCollection
{

    private $topArticles;
    private $topAuthorArticles;


    /**
     * @return mixed
     */
    public function getTopArticles() : array
    {
        return $this->topArticles->articles();
    }

    /**
     * @param Ranking $topArticles
     */
    public function setTopArticles(Ranking $topArticles)
    {
        $this->topArticles = $topArticles;
    }

    /**
     * @return mixed
     */
    public function getTopAuthorArticles(): array
    {
        return $this->topAuthorArticles->articles();
    }

    /**
     * @param Ranking $topAuthorArticles
     */
    public function setTopAuthorArticles(Ranking $topAuthorArticles)
    {
        $this->topAuthorArticles = $topAuthorArticles;
    }
}