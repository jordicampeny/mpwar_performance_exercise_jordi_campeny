<?php

namespace Performance\Domain;

interface RankingRepository
{
    /**
     * @param int $quantity
     * @return Ranking
     */
    public function getTop(int $quantity);

    /**
     * @param string $authorId
     * @param int $quantity
     * @return Ranking
     */
	public function getAuthorTop($authorId, int $quantity);

    /**
     * @param string $articleId
     * @param string $authorId
     * @param int $quantity
     * @return mixed
     */
    public function increaseVisitsIn(string $articleId, string $authorId, int $quantity = 1);
}