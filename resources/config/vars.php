<?php

$app['asset_path'] = "";

$app["db.options.host"]     = "";
$app["db.options.user"]     = "";
$app["db.options.password"] = "";
$app["db.options.dbname"]   = "";

$app["S3.client.credentials.key"]    = "";
$app["S3.client.credentials.secret"] = "";
$app["S3.client.region"]             = "";
$app["S3.client.version"]            = "";
$app["S3.bucket"]                    = "";

$app['redis.client.host'] = "";
$app['redis.client.port'] = "";
$app['redis.client.scheme'] = "";


$app["imageBasePath"] = "";